for (var i = 0; i < 100; i++) {
    if (i % 3 === 0 && i % 5 !== 0) {
        console.log('Itelios');
    } else if (i % 5 === 0 && i % 3 !== 0) {
        console.log('Capgemini');
    } else if (i % 3 === 0 && i % 5 === 0) {
        console.log('Itelios, part of Capgemini');
    } else if (isPrime(i)) {
        console.log('Primo');
    }
}

function isPrime (n) {
    if (n === 2) {
        return true;
    } else if (n > 1) {
        for (var i = 2; i < n; i++) {
            if (n % i !== 0) {
                return true;
            } else if (n === i * i) {
                return false;
            } else {
                return false;
            }
        }
    } else {
        return false;
    }
}